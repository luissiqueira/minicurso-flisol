Projeto
-------
O projeto Django aqui disponibilizado foi desenvolvido com base no projeto do site http://www.aprendendodjango.com.br e tem como finalidade auxiliar na aprendizagem do Django.

Apresentação
------------
Foi disponibilizado também o slide usado na apresentação do minicurso, bom estudos.

Complementos
------------
Site oficial:
http://djangoproject.com
Comunidade Brasileira:
http://www.djangobrasil.org/
Recomendações:
http://www.aprendendodjango.com/
https://www.youtube.com/watch?v=dNJXN70Nqt0

> Django, um framework para perfeccionistas com pequenos prazos.