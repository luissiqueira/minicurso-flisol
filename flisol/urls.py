from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'core.views.home'),
    url(r'^artigo/(?P<artigo_id>\d+)/$', 'core.views.artigo'),

    url(r'^admin/', include(admin.site.urls)),
)
