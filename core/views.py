from django.shortcuts import render_to_response
from django.template import RequestContext

from .models import Artigo


def home(request):
    artigos = Artigo.objects.all()
    return render_to_response('index.html', locals(), RequestContext(request))


def artigo(request, artigo_id):
    artigo = Artigo.objects.get(id=artigo_id)
    return render_to_response('artigo.html', locals(), RequestContext(request))