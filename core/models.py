from django.db import models
from datetime import datetime


class Artigo(models.Model):
    titulo = models.CharField(max_length=100)
    conteudo = models.TextField()
    publicacao = models.DateTimeField(default=datetime.now, blank=True)

    def get_absolute_url(self):
        return '/artigo/%d/' % self.id

    def __unicode__(self):
        return self.titulo

    class Meta:
        verbose_name = "Artigo"
        verbose_name_plural = "Artigos"
        ordering = ('-publicacao',)
